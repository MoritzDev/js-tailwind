// vite.config.js
const { resolve } = require('path')
module.exports = {
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                fun: resolve(__dirname, 'pages/fun.html'),
                rng: resolve(__dirname, 'pages/jstest/rng.html')
            }
        }
    }
}